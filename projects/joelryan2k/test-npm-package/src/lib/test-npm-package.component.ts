import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-test-npm-package',
  template: `
    <p>
      test-npm-package works!
    </p>
  `,
  styles: [
  ]
})
export class TestNpmPackageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
