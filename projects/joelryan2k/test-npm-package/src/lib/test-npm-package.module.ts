import { NgModule } from '@angular/core';
import { TestNpmPackageComponent } from './test-npm-package.component';



@NgModule({
  declarations: [
    TestNpmPackageComponent
  ],
  imports: [
  ],
  exports: [
    TestNpmPackageComponent
  ]
})
export class TestNpmPackageModule { }
