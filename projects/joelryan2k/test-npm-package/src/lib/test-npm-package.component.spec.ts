import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestNpmPackageComponent } from './test-npm-package.component';

describe('TestNpmPackageComponent', () => {
  let component: TestNpmPackageComponent;
  let fixture: ComponentFixture<TestNpmPackageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestNpmPackageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestNpmPackageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
