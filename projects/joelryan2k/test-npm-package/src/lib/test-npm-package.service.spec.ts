import { TestBed } from '@angular/core/testing';

import { TestNpmPackageService } from './test-npm-package.service';

describe('TestNpmPackageService', () => {
  let service: TestNpmPackageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestNpmPackageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
