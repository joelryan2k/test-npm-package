/*
 * Public API Surface of test-npm-package
 */

export * from './lib/test-npm-package.service';
export * from './lib/test-npm-package.component';
export * from './lib/test-npm-package.module';
